# RISC-V CPU superscalar microarchitecture design
## Description
This repository contains source and simulation code created as the output of the RISC-V CPU superscalar microarchitecture design bachelor thesis.
## Requirements
1. [Vivado ML Edition](https://www.xilinx.com/support/download.html)
2. [Python 3.10>=](https://www.python.org/downloads/)
3. riscv64-unknown-elf-gcc
4. riscv64-unknown-elf-binutils
5. [Git](https://git-scm.com/downloads)
6. [Cmake](https://cmake.org/download/)

## Installation
```
git clone https://gitlab.fit.cvut.cz/veznitom/VTM-official
cd VTM-official
git submodule update --init --recursive
./build-tests.sh
python3.x elfhex.py
```
## Vivado Simulation
1. Create new Vivado project `File->Project->New` or use Quick Start.
2. Name the project.

    ![img](/imgs/new_project.png)
4. Select `RTL Project` and check `Do not specify sources at this time`.

    ![img](/imgs/new_project_2.png)
4. `Boards` and select any available board (preferably Artix-7 AC701).

    ![img](/imgs/boards.png)
5. `Add Sources`.

    ![img](/imgs/sources.png)

6. `Next`.

    ![img](/imgs/add_sources.png)

7. `Add Directories`. And select the path to the `VTM-official/src` directory.

    ![img](/imgs/source_dir.png)

8. `Finish`.
9. `Add Sources`.

    ![img](/imgs/sim_sources.png)
10. `Add Directories`. And select the path to the `VTM-official/sim` directory.

    ![img](/imgs/sim_dir.png)
11. `Finish`.
12. Reorder the compilation order.

    ![img](/imgs/compile_order.png)
13. Select desired simulation file, right-click the simulation file and select `Set as Top` to make the file a simulation target.
    
    ![img](/imgs/hierarchy.png)
14. Change the `absolute-path` in path portion of the desired testbench to the absolute path to the test.
15. `Flow->Run Simulation->Run Behavioural Simulation` or use the Flow Navigator.

    ![img](/imgs/flow_navigator.png)
16. The simulation starts and opens waveform diagram. The Vivado should automatically load pre configured waveform diagram configurations for the Top and TopTest testbenches if not repeat the same steps for loading the simulation files but instead load files and select `VTM-official/sim/Top.wcfg` and `VTM-official/sim/Top.wcfg`.
